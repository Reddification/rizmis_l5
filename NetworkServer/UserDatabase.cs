﻿using CryptographyUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace NetworkServer
{
    public class UserDatabase
    {
        private Dictionary<string, RsaKeyDataExtract> knownUsers;

        public UserDatabase()
        {
            knownUsers = new Dictionary<string, RsaKeyDataExtract>();
        }
        public bool CheckUser(string login, RSAParameters keyData) 
            => knownUsers.Keys.Contains(login) && knownUsers[login].Modulus == keyData.Modulus && knownUsers[login].Exponent == keyData.Exponent;

        internal void AddUser(string login, RsaKeyDataExtract keyData)
        {
            if (!knownUsers.Keys.Any(a => a == login))
                this.knownUsers.Add(login, keyData);
        }

        internal RsaKeyDataExtract GetUserPublicKey(string login)
        {
            return this.knownUsers[login];
        }
    }
}
