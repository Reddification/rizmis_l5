﻿using CryptographyUtils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utilities;

namespace NetworkServer
{
    //https://www.c-sharpcorner.com/article/socket-programming-in-C-Sharp/
    public class AuthServer: SocketWrapperBase, IDisposable
    {
        private UserDatabase database;
        private string name = "socket server";
        private Dictionary<Socket, string> socketLoginMapping = new Dictionary<Socket, string>();

        private Socket socket;
        private readonly int port;
        private readonly ILogger logger;
        private TaskFactory tFactory = new TaskFactory(TaskCreationOptions.LongRunning, TaskContinuationOptions.None);
        public AuthServer(int port, ILogger logger = null, int defaultBufferSize = 4 * 1024)
        {
            database = new UserDatabase();
            this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            this.port = port;
            this.defaultBufferSize = defaultBufferSize;
            this.logger = logger;
        }

        ~AuthServer()
        {
            foreach (var item in socketLoginMapping)
            {
                if (item.Key.Connected)
                    item.Key.Shutdown(SocketShutdown.Both);
                item.Key.Close();
                item.Key.Dispose();
            }

            this.Dispose();
        }


        public void StartListening()
        {
            IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Loopback, port);
            socket.Bind(localEndPoint);
            logger?.Log("Binded to " + localEndPoint.ToString());
            socket.Listen(10);
            logger?.Log("Started listening, max 10 connections");

            while (true)
            {
                Socket handler = socket.Accept();
                logger?.Log("Accepting request");
                startCommunication(handler);
            }
        }

        private void startCommunication(Socket handler)
        {
            tFactory.StartNew(() =>
            {
                BinaryFormatter bf = new BinaryFormatter();
                bool requestResult = true;

                do
                {
                    NetworkMessage nm = readMessage(handler, bf);
                    logger?.Log($"Received message of type: {nm.Control} from someone who claims to be {nm.Login}");
                    requestResult = handleRequest(handler, bf, nm);
                } while (handler.Connected && requestResult);

                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
                handler.Dispose();
            });
        }

        private bool handleRequest(Socket handler, BinaryFormatter bf, NetworkMessage nm)
        {
            switch (nm.Control)
            {
                case ControlMessage.Authenticate:
                    break;
                case ControlMessage.Introduction:
                    Credentials nc = (Credentials)nm.Data;
                    int serverRandom = Cryptos.GenerateRandomNumber();

                    logger?.Log($"Starting challenge with {nm.Login}. Sending random = {serverRandom}");
                    handler.Send(prepareMessage(bf, serverRandom, ControlMessage.ServerRandomChallenge, this.name)); //send server random
                    logger?.Log("Sent random challenge");
                    NetworkMessage responseRandomSigned = readMessage(handler, bf); //receive encrypted random
                    if (responseRandomSigned.Control == ControlMessage.ServerRandomChallenge)
                    {
                        int clientInt = Convert.ToInt32(responseRandomSigned.Data);
                        if (clientInt != serverRandom)
                        {
                            logger?.Log("AAAAAA");
                            return false;
                        }

                        if (Cryptos.VerifySignature(BitConverter.GetBytes(clientInt), responseRandomSigned.Signature, (RSAParameters)nc.KeyData))
                        {
                            logger?.Log($"Challenge success, adding user {responseRandomSigned.Login} to database");
                            this.database.AddUser(nc.Login, nc.KeyData);
                            handler.Send(prepareMessage(bf, null, ControlMessage.Confirmation, name));
                        }
                        else
                        {
                            handler.Send(prepareMessage(bf, "I fak yer bullshit shit", ControlMessage.Error, name));
                            return false;
                        }
                    }
                    else handleRequest(handler, bf, responseRandomSigned);

                    break;
                case ControlMessage.ServerRandomChallenge:
                    break;
                case ControlMessage.Content:
                    RSAParameters publicKey = (RSAParameters)database.GetUserPublicKey(nm.Login);
                    string content = Encoding.Default.GetString((byte[])nm.Data);

                    if (Cryptos.VerifySignature((byte[])nm.Data, nm.Signature, publicKey))
                    {
                        logger?.Log($"Successfully verified signature from {nm.Login}, message: {content}");
                    }
                    else
                    {
                        logger?.Log($"Failed to verify signature for content [{content}] from {nm.Login}");
                        return false;
                    }

                    break;
                case ControlMessage.Shutdown:
                    break;
                default:
                    break;
            }

            return true;
        }

        public void Dispose()
        {
            this.socket.Shutdown(SocketShutdown.Both);
            this.socket.Close();
            this.socket.Dispose();
        }
    }
}
