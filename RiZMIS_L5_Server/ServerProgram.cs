﻿using NetworkServer;
using System;
using System.Threading.Tasks;
using Utilities;

namespace RiZMIS_L5_Server
{
    class ServerProgram
    {
        static void Main(string[] args)
        {
            AuthServer server = new AuthServer(61844, new ConsoleLogger());
            Task serverTask = Task.Run(() =>
            {
                server.StartListening();
            });

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
    }
}
