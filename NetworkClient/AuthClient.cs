﻿using CryptographyUtils;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using Utilities;

namespace NetworkClient
{
    public class AuthClient:SocketWrapperBase, IDisposable
    {
        private string login;
        private RSAParameters keyData;

        private Socket socket;
        private IPAddress serverAddress;
        private int serverPort;
        private readonly ILogger logger;

        public AuthClient(IPAddress serverAddress, int serverPort, string login, ILogger logger = null, bool openConnection = false)
        {
            this.serverAddress = serverAddress;
            this.defaultBufferSize = 4 * 1024;
            this.serverPort = serverPort;
            this.logger = logger;
            this.login = login;
            if (openConnection)
                Connect();
        }

        public void Connect()
        {
            IPEndPoint remoteEP = new IPEndPoint(serverAddress, serverPort);
            this.socket = new Socket(serverAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            this.socket.Connect(remoteEP);
            this.logger?.Log("Connected to the server");
        }

        public void Dispose()
        {
            this.socket.Shutdown(SocketShutdown.Both);
            this.socket.Close();
            this.socket.Dispose();
        }

        public void GenerateKeys()
        {
            this.keyData = Cryptos.GenerateRsaKeys();
            logger?.Log($"Generated RSA keys:\nE = {BitConverter.ToString(keyData.Exponent)}\nn = {BitConverter.ToString(keyData.Modulus)}");
        }

        public void SendContent(string v)
        {
            byte[] binaryContent = Encoding.Default.GetBytes(v);
            byte[] signature = Cryptos.CreateSignature(binaryContent, this.keyData);

            this.SendData(new BinaryFormatter(), binaryContent, ControlMessage.Content, signature);
        }

        public bool SendPublicCreds()
        {
            BinaryFormatter bf = new BinaryFormatter();

            Credentials creds = new Credentials()
            {
                Login = this.login,
                KeyData = new RsaKeyDataExtract(keyData)
            };

            this.SendData(bf, creds, ControlMessage.Introduction);
            logger?.Log("Sent data: " + ControlMessage.Introduction);

            //get servers random number challenge
            NetworkMessage nm = readMessage(socket, bf);
            logger?.Log($"Received message from {nm.Login} of type {nm.Control}");
            if (nm.Control == ControlMessage.ServerRandomChallenge)
            {
                logger?.Log("Server requested random challenge");
                int serverRandom = Convert.ToInt32(nm.Data);
                logger?.Log("Server's random: " + serverRandom.ToString());

                byte[] randomSignature = Cryptos.CreateSignature(BitConverter.GetBytes(serverRandom), this.keyData);
                this.SendData(bf, serverRandom, ControlMessage.ServerRandomChallenge, randomSignature);
                NetworkMessage challengeResult = this.readMessage(socket, bf); //wait for confirmation
                return challengeResult.Control == ControlMessage.Confirmation;
            }
            else
            {
                processServerMessage(nm);
                return false;
            }
        }

        private void processServerMessage(NetworkMessage nm)
        {
            switch (nm.Control)
            {
                case ControlMessage.ServerRandomChallenge:
                    break;
                case ControlMessage.Error:
                    logger?.Log($"Server error: {nm.Data}");
                    break;
                case ControlMessage.Authenticate:
                case ControlMessage.Introduction:
                case ControlMessage.Content:
                case ControlMessage.Shutdown:
                default:
                    break;
            }
        }

        private void SendData(BinaryFormatter bf, object data, ControlMessage cm, byte[] signature = null)
        {
            byte[] message = prepareMessage(bf, data, cm, this.login, signature);
            socket.Send(message);
            logger?.Log($"Sent {cm} data");
        }
    }
}
