﻿using NetworkClient;
using NetworkServer;
using System;
using System.Net;
using System.Threading.Tasks;
using Utilities;

namespace RiZMIS_L5
{
    class ClientProgram
    {
        static void Main(string[] args)
        {
            AuthClient client = new AuthClient(IPAddress.Loopback, 61844, "user A", new ConsoleLogger());
            
            Task clientTask = Task.Run(() => 
            {
                client.GenerateKeys();
                client.Connect();
                if (client.SendPublicCreds())
                {
                    Console.WriteLine("Successfully exchanged public credentials");
                }

                client.SendContent("Secret message, encrypted signed whatever");
            });

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }
    }
}
