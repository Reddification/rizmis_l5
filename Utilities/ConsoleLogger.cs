﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities
{
    public class ConsoleLogger : ILogger
    {
        public void Log(string logData, bool appendTime = true)
        {
            if (appendTime)
                Console.Write($"[{DateTime.Now.ToLongTimeString()}]");
            Console.WriteLine(logData);
        }
    }
}
