﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Utilities
{
    public enum ControlMessage
    {
        Authenticate = 0x100,
        Introduction = 0x200,
        ServerRandomChallenge = 0x300,
        Content = 0x400,
        Shutdown = 0x500,
        Error = 0x600,
        Confirmation = 0x700
    }

    [Serializable]
    public class NetworkMessage
    {
        public ControlMessage Control { get; set; }
        public object Data { get; set; }
        /// <summary>
        /// Signature for hash of the data
        /// </summary>
        public byte[] Signature { get; set; }
        public string Login { get; set; }
    }
}
