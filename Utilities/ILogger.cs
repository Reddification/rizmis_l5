﻿using System;

namespace Utilities
{
    public interface ILogger
    {
        void Log(string logData, bool appendTime=true);
    }
}
