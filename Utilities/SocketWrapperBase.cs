﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Utilities
{
    public abstract class SocketWrapperBase
    {
        protected int defaultBufferSize = 1024;

        protected NetworkMessage readMessage(Socket handler, BinaryFormatter bf)
        {
            byte[] buffer = new byte[defaultBufferSize];
            using (MemoryStream messageStream = new MemoryStream())
            {
                do
                {
                    handler.Receive(buffer);
                    messageStream.Write(buffer, 0, buffer.Length);
                } while (handler.Available > 0);

                messageStream.Seek(0, SeekOrigin.Begin);
                NetworkMessage nm = (NetworkMessage)bf.Deserialize(messageStream);
                return nm;
            }
        }
        
        protected byte[] prepareMessage(BinaryFormatter bf, object data, ControlMessage cm, string login, byte[] signature = null)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, new NetworkMessage()
                {
                    Control = cm,
                    Data = data,
                    Signature = signature,
                    Login = login
                });
                return ms.ToArray();
            }
        }
    }
}
