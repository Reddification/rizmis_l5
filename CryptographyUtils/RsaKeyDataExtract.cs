﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace CryptographyUtils
{
    [Serializable]
    public struct RsaKeyDataExtract
    {
        public byte[] Exponent;
        public byte[] Modulus;

        public RsaKeyDataExtract(RSAParameters rsas)
        {
            this.Exponent = rsas.Exponent;
            this.Modulus = rsas.Modulus;
        }

        public static explicit operator RSAParameters(RsaKeyDataExtract me)
        {
            return new RSAParameters()
            {
                Exponent = me.Exponent,
                Modulus = me.Modulus
            };
        }
    }
}
