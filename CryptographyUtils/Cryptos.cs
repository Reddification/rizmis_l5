﻿using System;
using System.Security.Cryptography;

namespace CryptographyUtils
{
    public static class Cryptos
    {
        public static RSAParameters GenerateRsaKeys()
        {
            using (RSACryptoServiceProvider crypto = new RSACryptoServiceProvider())
            {
                //https://docs.microsoft.com/en-us/dotnet/api/system.security.cryptography.rsaparameters?view=netcore-3.1
                return crypto.ExportParameters(true);
            }
        }

        public static RSAParameters ExtractPublicKeyData(ref RSAParameters allData)
        {
            RSAParameters res = new RSAParameters();
            res.Modulus = allData.Modulus;
            res.Exponent = allData.Exponent;
            return res;
        }

        public static byte[] ComputeHash(byte[] content)
        {
            using (var hasher = SHA512.Create())
                return hasher.ComputeHash(content);
        }

        public static int GenerateRandomNumber()
        {
            using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
            {
                byte[] buffer = new byte[4];
                rng.GetNonZeroBytes(buffer);
                return BitConverter.ToInt32(buffer, 0);
            }
        }

        //http://www.visualstudiotutorial.net/digital-signature-implementation
        public static byte[] CreateSignature(byte[] data, RSAParameters keyData)
        {
            using (RSACryptoServiceProvider crypto = new RSACryptoServiceProvider())
            {
                crypto.ImportParameters(keyData);
                return crypto.SignHash(ComputeHash(data), HashAlgorithmName.SHA512, RSASignaturePadding.Pkcs1);
            }
        }

        public static bool VerifySignature(byte[] data, byte[] signature, RSAParameters keyData)
        {
            using (RSACryptoServiceProvider crypto = new RSACryptoServiceProvider())
            {
                crypto.ImportParameters(keyData);
                return crypto.VerifyHash(ComputeHash(data), signature, HashAlgorithmName.SHA512, RSASignaturePadding.Pkcs1);
            }
        }

        public static byte[] EncryptDataRSA(byte[] data, RSAParameters keyData)
        {
            using (RSACryptoServiceProvider crypto = new RSACryptoServiceProvider())
            {
                crypto.ImportParameters(keyData);
                return crypto.Encrypt(data, false);
            }
        }

        public static byte[] DecryptDataRSA(byte[] data, RSAParameters keyData)
        {
            using (RSACryptoServiceProvider crypto = new RSACryptoServiceProvider())
            {
                crypto.ImportParameters(keyData);
                return crypto.Decrypt(data, false);
            }
        }
    }
}
