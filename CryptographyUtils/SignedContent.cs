﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryptographyUtils
{
    [Serializable]
    public class SignedContent
    {
        public byte[] Signature { get; set; }
        public byte[] Content { get; set; }
    }
}
