﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace CryptographyUtils
{
    [Serializable]
    public class Credentials
    {
        public string Login { get; set; }
        /// <summary>
        /// Contains only public key components
        /// </summary>
        public RsaKeyDataExtract KeyData { get; set; }
    }
}
